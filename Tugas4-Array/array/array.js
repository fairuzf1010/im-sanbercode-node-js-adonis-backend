
console.log('SOAL 1 Array Multidimensi :');

function dataHandling(params) {
    
    let kata = '';
    let nomor_id = '';
    let nama_lengkap = '';
    let ttl = '';
    let hobi = '';

    n = params.length;

    for (let index = 0; index < n; index++) {
  
        nomor_id = 'Nomor ID :' + params[index][0] + '\n';
        nama_lengkap  = 'Nama Lengkap :' + params[index][1] + '\n';
        ttl = 'TTL :' + params[index][2] + '\n';
        hobi = 'Hobi :' + params[index][3] + '\n';
    
        kata += nomor_id + nama_lengkap + ttl + hobi + '\n \n';
    
        // console.log(kata);

        // console.log(kata);

        // return kata;
        
        // console.log(kata);
    }


    return kata;
 
    
}

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 



// console.log(input.length);

console.log(dataHandling(input));
console.log('\n');
// dataHandling(input);



console.log('SOAL 2 :');


function balikKata(parameter) {
 
    let kata_akhir = '';

    for (let index_kata = parameter.length - 1; index_kata >= 0; index_kata--) {
         
        kata_akhir = kata_akhir + parameter[index_kata];
        
    }
    

    return kata_akhir;


}


console.log(balikKata("SanberCode")) ;
// Output: edoCrebnaS

console.log(balikKata("racecar")) ;
// Output: racecar

console.log(balikKata("kasur rusak"));
// Output: kasur rusak

console.log(balikKata("haji ijah"));
// Output: haji ijah

console.log(balikKata("I am Sanbers"));
// Output: srebnaS ma I
 console.log('\n');







