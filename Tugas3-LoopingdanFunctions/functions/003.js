function introduce(name, age, address, hobby) {
    // your code

    var kalimat = 'Nama saya ' + name + ', umur saya ' + age + ' tahun, alamat saya di ' + address + ', dan saya punya hobby yaitu ' + hobby;
    return kalimat;

  }
  
   
   // TEST CASES
  console.log(introduce("Agus", 30, "Jogja", "Gaming")) // Menampilkan "Nama saya Agus, umur saya 30 tahun, alamat saya di Jogja, dan saya punya hobby yaitu Gaming!" 
