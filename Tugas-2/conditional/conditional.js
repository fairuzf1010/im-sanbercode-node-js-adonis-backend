//SOAL 1 
console.log('SOAL 1 : \n');
var nama = "John";
var peran = "";


if (nama == '' && peran == '') {
    
    console.log('Nama Harus Diisi !');
} 


else if(nama != '' && peran == '')
{
    var kata0 = "Halo " + nama + ", Pilih peranmu untuk memulai game!";

    console.log(kata0);


}

else if(nama != '' && peran == 'Penyihir')
{
    var kata1 = "Halo " + peran + ' ' + nama + ", kamu dapat melihat siapa yang menjadi werewolf!";

    console.log(kata1);

}

else if(nama != '' && peran == 'Guard')
{

    var kata2 = "Halo " + peran + ' ' + nama + ", kamu akan membantu melindungi temanmu dari serangan werewolf";

    console.log(kata2);

}

else if(nama != '' && peran == 'Werewolf')
{


    var kata3 = "Halo " + peran + ' ' + nama + ", Kamu akan memakan mangsa setiap malam!";

    console.log(kata3);
}


else {
 
    console.log('Tidak Ada');
    
}




//SOAL 2 
console.log('\n');

console.log('SOAL 2 : \n');


var hari = 21;
var bulan = 12;
var tahun = 1945 ;

var month = '';

switch (bulan) {
    case 1:
        month = 'Januari';
        break;
    
    case 2:
        month = 'Februari'; 
        break;

    case 3:
        month = 'Maret'; 
        break;

    case 4:
        month = 'April'; 
        break;

    case 5:
        month = 'Mei'; 
        break;

    case 6:
        month = 'Juni'; 
        break;

    case 7:
        month = 'Juli'; 
        break;

    case 8:
        month = 'Agustus'; 
        break;

    case 9:
        month = 'September'; 
        break;

    case 10:
        month = 'Oktober'; 
        break;

    case 11:
        month = 'November'; 
        break;

    case 12:
        month = 'Desember'; 
        break;
    
    default:
        month = 'Tidak Ada'
        break;
}


var result_date = hari + ' ' + month + ' ' + tahun;

console.log(result_date);
